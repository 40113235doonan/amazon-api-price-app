<?php
    session_start();
    if(isset($_SESSION["loggedin"])) {
        $loggedin = true;
        $username = $_SESSION["loggedin"];
    }
    else {
        $loggedin = false;
    }
?>



<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">


<!-- Bootstrap core CSS -->
<link href="public/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <title>Price Assistant | Home</title>



    <!-- Custom CSS -->
    <link href="public/bootstrap/css/style.css" rel="stylesheet">


</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="height:77px;">
        <div class="container" >
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
            <a class="navbar-brand" href="index.php"><img src="logo.png" style="width:350px;height:60px;"></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav" style="color:#FFFFFF; padding-left: 75%;">
                    <?php
                        if($loggedin == true) {
                            echo "<p>Signed in as " . $username . " - <a href='logout.php' style='color:#FFFFFF;'>Logout</a></p>";
                        }
                        else {
                            echo '<p><a href="login.php" style="color:#FFFFFF;">Sign in</a> or <a href="register.php" style="color:#FFFFFF;">Register</a></p>';
                        }
                    ?>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-md-3">
              <form action = "results.php">
              <input type="text" name="search" placeholder="Search.." style="width:100%; height:25px;; margin-top:10px;  color:#e24c2c;">
            </form>
            </div>

            <div class="col-md-9">

              <br></br>

        </div>

    </div>
    <!-- /.container -->

    <div class="container">

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Group Fifty One 2017</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="public/bootstrap/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="public/bootstrap/js/bootstrap.min.js"></script>

</body>

</html>
