<?php 


use App\Controllers\HomeController;
use App\Controllers\UserController;

$app->group('/', function(){
	$this->get('', HomeController::class . ':index');
});


$app->group('/users', function() {
	$this->get('', UserController::class . ':index');
});
