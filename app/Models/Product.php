<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\UserProduct;
use App\Models\ProductData;
use App\Models\ProductSource;

class Product extends Model {
	
	protected $table = 'product';
	
	protected $fillable = [
		'asin',
		'name',
		'short_desc',
		'img_link',
		'active',
		'prod_link',
		'user_id'
	];
	
	
	public function savedProducts() {
		
		return $this->hasMany(UserProduct::class, 'product_id', 'product_id');
		
	}
	
	public function getIdByAsin($asin) {
		
		return $this->where('asin', $asin)->first();
		
	}
	
/* 	public function marketData() {
		
		return $this->belo(ProductSource::class, 'product_market_data', 'product_source_id', 'product_id');
		
	}
 */
	
};