<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\UserProduct;
use App\Models\UserRole;
use App\Models\Product;

class User extends Model {
	
	protected $table = 'users';
	
	protected $fillable = [
		'username',
		'firstname',
		'lastname',
		'email',
		'password',
		'active',
		'hash',
		'user_roles_iduser_roles',
	];
	
	
	public function savedProducts() {
		
		return $this->hasMany(UserProduct::class);
		
	}
	
	public function userRole() {
		
		return $this->belongsTo(UserRole::class);
		
	}

	public function getUserIdByUsername($username) {
	
		return $this->where('username', '=' , $username)->first()->user_id;
	
	}
	
	public function getUserSavedProducts() {
		
		//$user_id = $this->getUserIdByUsername($username);
		
		return $this->product->where('user_id', 4)->get();
		
	}
	
};