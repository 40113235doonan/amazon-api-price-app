<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ProductData;
use App\Models\Product;

class ProductSource extends Model {

	protected $table = 'product_source';

	protected $fillable = [
			'link',
			'name',
			'active',
			'currency',
	];

	public function marketData() {
	
		return $this->belongsToMany(Product::class, 'product_market_data', 'product_source_id', 'product_id');
	
	}
	



};