<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;
use App\Model\ProductSource;

class ProductData extends Model {

	protected $table = 'product_market_data';

	protected $fillable = [
			'product_id',
			'product_source_id',
			'product_price',
			'currency',
	];
	
public function addData($request, $response, $args) {
		
		// US 1, UK 2
		
		$source = $args['source'];
		$product_id = $args['product_id'];
		$price = $args['price'];
		$currency = $this->c->productsource->where('product_source_id', $source)->currency->first();
		
		self::create([
			'product_id' => $product_id,
			'product_source_id' => $source,
			'product_price' => $price,
			'currency' => $currency
		]);
		
		
	}

};