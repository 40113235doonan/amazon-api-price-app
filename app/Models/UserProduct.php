<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Model\User;
use App\Model\Product;

class UserProduct extends Model {

	protected $table = 'user_saved_product';

	protected $fillable = [
			'user_id',
			'product_id',
	];
	
	public function user() {
		
		return $this->belongsTo(User::class);
		
	}
	
	public function product() {
		
		return $this->belongsTo(Product::class);
		
	}



};