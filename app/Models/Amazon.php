<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Product;
use App\Models\ProductSource;

class Amazon extends Model {

	protected $table = 'product';

	protected $fillable = [
			'asin',
			'name',
			'short_desc',
			'img_link',
			'active',
			'prod_link',
	];
	
	public $endpoint;
	public $search;
	public $result;

	
	public function __construct($endpoint, $search) {
		
		if ($endpoint == '') {
			$this->endpoint = "webservices.amazon.co.uk";
		} else {
			$this->endpoint = $endpoint;
		}
		
		if ($search == '') {
			$this->search = "batman";
		} else {
			$this->search = $search;
		}
		
		// Your AWS Access Key ID, as taken from the AWS Your Account page
		$aws_access_key_id = "AKIAJ3HHY2IW6CIKWP3A";
		
		// Your AWS Secret Key corresponding to the above ID, as taken from the AWS Your Account page
		$aws_secret_key = "LPc6OzzYrXniNXfPUmTk6sXliSynqXmgaV1TToJT";
		
		$uri = "/onca/xml";
		
		$params = array(
				"Service" => "AWSECommerceService",
				"Operation" => "ItemSearch",
				"AWSAccessKeyId" => "AKIAJ3HHY2IW6CIKWP3A",
				"AssociateTag" => "zeso0e-21",
				"SearchIndex" => "All",
				"ResponseGroup" => "Images,ItemAttributes,Offers",
				"Keywords" => $this->search,
		);
		
		
		// Set current timestamp if not set
		if (!isset($params["Timestamp"])) {
			$params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
		}
		
		// Sort the parameters by key
		ksort($params);
		
		$pairs = array();
		
		foreach ($params as $key => $value) {
			array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
		}
		
		// Generate the canonical query
		$canonical_query_string = join("&", $pairs);
		
		// Generate the string to be signed
		$string_to_sign = "GET\n".$this->endpoint."\n".$uri."\n".$canonical_query_string;
		
		// Generate the signature required by the Product Advertising API
		$signature = base64_encode(hash_hmac("sha256", $string_to_sign, $aws_secret_key, true));
		
		// Generate the signed URL
		$request_url = 'http://'.$this->endpoint.$uri.'?'.$canonical_query_string.'&Signature='.rawurlencode($signature);
		
		
		//echo "Signed URL: \"".$request_url."\"";
		$xml_string = file_get_contents($request_url);
		$xml = simplexml_load_string($xml_string);
		$json = json_encode($xml);
		$array = json_decode($json,TRUE);
		
		$this->result = $json;
	}
};