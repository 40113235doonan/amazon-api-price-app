<?php 

namespace App\Controllers;


class HomeController extends Controller {
	
	public function index($request, $response, $args) {
		
		return $this->c->view->render($response, 'layouts/base.twig');
		
	}
	
	public function show() {
		
		return 'Single user';
	}
}