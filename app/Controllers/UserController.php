<?php 

namespace App\Controllers;

use App\Models\User;

class UserController extends Controller {

	public function index($request, $response, $args) {

		$users = $this->c->user->get();

		return $response->withJson($users);

	}
	

	public function getUserSavedProducts($request, $response, $args) {
		
		$user_id = $this->c->user->where('username', $args['username'])->first()->user_id;
		
		
	
		return $response->withJson($this->c->product->where('user_id', $user_id)->get());
	
	}
	

	
}