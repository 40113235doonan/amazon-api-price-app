<?php

namespace App\Controllers;

class UserProductController extends Controller {

	public function index($request, $response, $args) {

		$data = $this->c->userproduct->get();

		return $response->withJson($data);

	}

}