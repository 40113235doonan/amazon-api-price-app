<?php

namespace App\Controllers;


use App\Models\Amazon;

class AmazonController extends Controller {

	public function results($request, $response, $args) {
		
		$result = $this->c->amazon->result;
	
		return $response->withJson($result);
	
	}
	
	public function search($request, $response, $args) {
		
		$search = $args['$keyword'];
		
		$result = new Amazon("webservices.amazon.co.uk", $search);
		
		return $response->withJson($result->result);
		
	}
}