<?php

namespace App\Controllers;

class ProductSourceController extends Controller {

	public function index($request, $response, $args) {

		$data = $this->c->productsource->get();

		return $response->withJson($data);

	}

}