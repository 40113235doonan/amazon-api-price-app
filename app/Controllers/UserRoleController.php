<?php

namespace App\Controllers;

class UserRoleController extends Controller {

	public function index($request, $response, $args) {

		$data = $this->c->userrole->get();

		return $response->withJson($data);

	}

}