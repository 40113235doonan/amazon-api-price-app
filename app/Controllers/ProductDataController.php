<?php

namespace App\Controllers;

class ProductDataController extends Controller {

	public function index($request, $response, $args) {

		$data = $this->c->productdata->get();

		return $response->withJson($data);

	}
	
	public function add($request, $response, $args) {
		
		$source = $args['source'];
		$product_id = $args['product_id'];
		$price = $args['price'];
		$currency = $this->c->productsource->where('product_source_id', $source)->first();
		
		$data = $this->c->productdata;
		
		$data::create([
				'product_id' => $product_id,
				'product_source_id' => $source,
				'product_price' => $price,
				'currency' => $currency->currency
		]);
		
	}

}