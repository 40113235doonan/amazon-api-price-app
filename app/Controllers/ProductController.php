<?php 

namespace App\Controllers;


class ProductController extends Controller {
	
	public function index($request, $response, $args) {
		
		$products = $this->c->product->get();
		
		return $response->withJson($products);
	
		//return $this->c->view->render($response, 'forms/product_form.twig');
	
	}
	
	public function show_single($request, $response, $args) {
				
		$product = $this->c->product->getIdByAsin($args['asin']);
		
		return $response->withJson($product);
		
	}
	
	public function delete_single($request, $response, $args) {
		//$product = $this->getProductById($args['id']);
		$this->c->product->where('product_id', $args['product_id'])->delete();
	}
	
	public function add_single($request, $response, $args) {
		
		$json = $request->getBody();
		$params = array_filter(json_decode($json, true));
		
		$product = $this->c->product;
		
		$user = $this->c->user;
		$username = $_SESSION["loggedin"];
		
 		if (!$this->ifExists($params['asin'])) {

 			$product::create([
 					'active' => 1,
 					'asin' => $params['asin'],
 					'name' => $params['name'],
 					'short_desc' => $params['short_desc'],
 					'prod_link' => $params['prod_link'],
 					'img_link' => $params['img_link'],
 					'user_id' => $user->getUserIdByUsername($username)
 			]);
			
 			return $response->withRedirect( '/' );
 			
		} else {
			return $response->withRedirect( '/error' );
		}
		
		
	}
	
	protected function getProductById($id) {
		return $this->c->product->where('id', $id)->get();
	}
	
	public function ifExists($asin) {
		
		$product = $this->c->product;
	
		if ($product::where('asin', '=', $asin)->exists()) {
			return true;
		} else {
			return false;
		}
	
	}

	
	
}