<?php

require __DIR__ . '/../vendor/autoload.php';


use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Pimple\Container;

$app = new \Slim\App([
		'settings' => [
				'displayErrorDetails' => true,
				'db' => [
						'driver' => 'mysql',
						'host' => 'localhost',
						'database' => 'slim',
						'username' => 'root',
						'password' => '',
						'charset' => 'utf8',
						'collation' => 'utf8_unicode_ci',
						'prefix' => ''
				]
		]
]);

// Bootstrap Eloquent ORM
$connFactory = new \Illuminate\Database\Connectors\ConnectionFactory();
$conn = $connFactory->make($settings);
$resolver = new \Illuminate\Database\ConnectionResolver();
$resolver->addConnection('default', $conn);
$resolver->setDefaultConnection('default');
\Illuminate\Database\Eloquent\Model::setConnectionResolver($resolver);

$container = $app->getContainer();

$container['db'] = function($c) {
	
	$capsule = new \Illuminate\Database\Capsule\Manager;
	$capsule->addConnection($c['settings']['db']);

	$capsule->setAsGlobal();
	$capsule->bootEloquent();

	return $capsule;
};

$container['view'] = function($container) {
	$view = new \Slim\Views\Twig(__DIR__ . '/../resources/views', [
			'cache' => false,
	]);
	
	$basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
	$view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));
	
	return $view;
};

$app->get('/products', function() {
	$products = \Product::all();
	echo $products->toJson();
});



