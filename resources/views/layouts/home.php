<?php
require_once 'init.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">


<!-- Bootstrap core CSS -->
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <title>Price Assistant | Home</title>
    
    <!-- Bootstrap Core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="bootstrap/css/style.css" rel="stylesheet">
    
    
              <!-- jQuery -->
              <script src="bootstrap/js/jquery.js"></script>

              <!-- Bootstrap Core JavaScript -->
              <script src="bootstrap/js/bootstrap.min.js"></script>



</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="height:77px;">
        <div class="container" >
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
            <a class="navbar-brand" href="/"><img src="logo.png" style="width:350px;height:60px;"></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav" style="color:#FFFFFF; padding-left: 75%;">
                    <?php
                        if($loggedin == true) {
                            echo "<p>Signed in as " . $username . ". <a href='logout.php' style='color:#FFFFFF;'>Logout</a></p>";
                        }
                        else {
                            echo '<p><a href="login.php" style="color:#FFFFFF;">Sign in</a> or <a href="register.php" style="color:#FFFFFF;">Register</a></p>';
                        }
                    ?>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>


    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-md-3">
              <form action = "results.php">
              <input type="text" name="search" placeholder="Search.." style="width:100%; height:25px;; margin-top:10px;  color:#e24c2c;">
            </form>
            </div>

            <div class="col-md-9">

              <br></br>

        </div>

    </div>
    <!-- /.container -->

    <div class="container">
    <div class="row">
				<div id="uk-results-container" class="col-md-4 col-lg-4">
					<strong style="color: #e24c2c; font-size: 200%;">Amazon.co.uk results</strong>
					<hr style="color: #000000; thickness: 10px;"></hr>
					<?php //getResults($array); ?>
				</div>
			
				<div id="us-results-container" class="col-md-4 col-lg-4">
					<strong style="color: #e24c2c; font-size: 200%;">Amazon.com results</strong>
					<hr style="color: #000000; thickness: 10px;"></hr>
					<?php //getResults($array1); ?>
				</div>
				<div id="saved-container" class="col-md-4 col-lg-4">
					<h3>Saved</h3>
				</div>
	
			</div>

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Group Fifty One 2017</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->
    
              <?php require_once 'jsinit.php'; ?>
              
              <!-- Custom JavaScript -->
    		<script type="text/javascript" src="js/save-product.js"></script>
    		<script type="text/javascript" src="js/loader.js"></script>

</body>

</html>
