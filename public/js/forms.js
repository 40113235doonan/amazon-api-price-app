// this is the id of the form
$("#add-product").submit(function(e) {

    var url = $("#add-product").attr('action'); // the script where you handle the form input.
    //$("#add-product").serializeJSON();
    var form = $("#add-product");
    var form_json = ConvertFormToJSON(form);
    
    
    console.log(JSON.stringify(form_json));
    
   $.ajax({
           type: "POST",
           url: url,
           data: JSON.stringify(form_json), // serializes the form's elements.
           datatype: 'json',
           success: function(data)
           {
               alert(data); // show response from the php script.
           }
    });

    e.preventDefault(); // avoid to execute the actual submit of the form.
});


function ConvertFormToJSON(form){
    var array = jQuery(form).serializeArray();
    var json = {};
    
    jQuery.each(array, function() {
        json[this.name] = this.value || '';
    });
    
    return json;
}