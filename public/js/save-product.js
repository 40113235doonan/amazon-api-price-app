
//$("#save-product").click(function(e) {


//var user_products_url = "http://amazonapp/user/products/";

$(document).ready(function() {
	
	var items = [];
	var products = [];
	

	
	items.push(JSON.parse(resultsUk).Items.Item);
	items.push(JSON.parse(resultsUs).Items.Item);

    //var item = JSON.parse(resultsUk).Items.Item;
    
	for (i=0; i < items.length; i++) {
	    $.each(items[i], function(k, v) {
	    	
	    	if (i == 0) {
	    		var holder="#uk-results-container";
	    	} else {
	    		var holder="#us-results-container";
	    	}
	    	
	    	setTimeout( function() {products.push(new product(
	    			v.ASIN,
	    			v.ItemAttributes.Title,
	    			'1',
	    			v.DetailPageURL,
	    			v.MediumImage.URL,
	    			v.OfferSummary.LowestNewPrice.FormattedPrice,
	    			holder
	    	))
	    	}, 1000);    	
	    });
	}
	
    
});

function product(asin, name, active, prod_link, img_link, prod_price, holder) {
	this.asin = asin;
	this.name = name;
	this.img_link = img_link;
	this.active = active;
	this.prod_link = prod_link;
	this.price = prod_price;

	this.itemHolder = "<div class='item col-md-12' id=" + this.asin + "></div>";

	$(holder).append(this.itemHolder);
	
	new itemPlaceholder(this);

};

function itemPlaceholder(product) {
	
	var user = readCookie('username');
	
	this.itemDetailHolder = "<div class='item-details-container'></div>";
	this.itemLink = "<h4><a class='item-title' href='' target='_blank'></a></h4>";
	this.asinHolder = "<div class='ASIN'></div>";
	this.imgHolder = "<div><img class='item-image' src=''></img></div>";
	this.priceHolder = "<div class='item-price'></div>";
	this.btnSave = "<button type='button' class='btn btn-save btn-primary'>Save</button><br><hr>";
	
	this.productDetails = [];
	this.productDetails.push(this.itemLink);
	this.productDetails.push(this.imgHolder);
	this.productDetails.push(this.priceHolder);
	this.productDetails.push(this.asinHolder);
	if (user) {
		this.productDetails.push(this.btnSave);
	}
	

	$(this.itemDetailHolder).appendTo('#' + product.asin);
	$('#' + product.asin).children('.item-details-container').append(this.productDetails);
	
	$('#' + product.asin).find('.ASIN').text(product.asin);
	$('#' + product.asin).find('.item-title').text(product.name);
	$('#' + product.asin).find('.item-title').attr('href', product.prod_link);
	$('#' + product.asin).find('.item-image').attr('src', product.img_link);
	$('#' + product.asin).find('.item-price').text(product.price);
	$('#' + product.asin).find('.btn-save').attr('id', product.asin + '-save');
	
	if (user) {
		new btnSave(product);
	}
	//alert(user);
	
	
	
	
	
}

function btnSave(product) {
	
	this.id = product.asin;
	
	$('#' + this.id + '-save').click( function(e) {

		//console.log(JSON.stringify(product));
		var baseUrl = readCookie('baseurl');
		var url = "http://" + baseUrl + "/products";

			    
		$.ajax({
			type : "POST",
			url : url,
			data : JSON.stringify(product), // serializes the form's elements.
			datatype : 'json',
			success : function(data) {
				//alert(url); // show response from the php script.
				console.log(JSON.stringify(product));
				
				alert('Saved to database!');
				addToSaved(product);
			},
			error: function(xhr, status, error) {
				//var err = eval("(" + xhr.responseText + ")");
				//alert(err.Message);
				alert(xhr.status + ' ' + error);
			}
		});
		
		e.preventDefault();
		
		//$('#' + this.id).attr('disabled', 'disabled');
	});
	

	
}

function btnDelete(asin) {
	
	this.id = '#' + asin + '-delete';	
	
	var baseUrl = readCookie('baseurl');
	var product_url = "http://" + baseUrl + "/products/" + asin;
	
	
	
	$(this.id).click( function(e) {
		
		$.getJSON(product_url,function(result) {
			
			var product_delete_url = "http://" + baseUrl + "/products/" + result.product_id;
			
			$.ajax({
				url: product_delete_url,
				type: 'DELETE',
				success: function(result) {
			        alert('Product removed from database!');
			    },
			    error: function(xhr, status, error) {
					//var err = eval("(" + xhr.responseText + ")");
					//alert(err.Message);
					alert(xhr.status + ' ' + error);
				}
			});
			
			
			
			
		});
		removeFromSaved(asin);
		e.preventDefault();
	});
	
}


function addToSaved(product) {
	var item = "<div id='" + product.asin + "-saved'><a href='" + product.prod_link + "' target='_blank'><h5>" + product.name + "</h5></a></div><button id='" + product.asin + "-delete' class='btn btn-danger'>Remove</button>";
	
	$('#' + product.asin + '-save').prop('disabled', true);
	$('#saved-container').append(item);
	
	btnDelete(product.asin);
	
}

function removeFromSaved(el) {
	$('#' + el + '-saved').remove();
	$('#' + el + '-delete').remove();
}

function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}










