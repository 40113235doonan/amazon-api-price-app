<?php


/**
 *
 */
class User
{
    /**
     *
     */
    public function __construct()
    {
    }

    /**
     * @var void
     */
    public $id;

    /**
     * @var void
     */
    public $username;

    /**
     * @var void
     */
    public $firstname;

    /**
     * @var void
     */
    public $lastname;

    /**
     * @var void
     */
    public $email;

    /**
     * @var void
     */
    public $password;

    /**
     * @var void
     */
    public $active;

    /**
     * @var void
     */
    public $createtime;

    /**
     * @var \UserRoles
     */
    public $user_role;

    /**
     *
     */
    public function add():void
    {
        // TODO: implement here
    }

    /**
     *
     */
    public function remove():void
    {
        // TODO: implement here
    }

    /**
     * @return void
     */
    public function getId():void
    {
        // TODO: implement here
        return null;
    }

    /**
     * @param void $value
     */
    public function setId($value):void
    {
        // TODO: implement here
    }

    /**
     * @return void
     */
    public function getUsername():void
    {
        // TODO: implement here
        return null;
    }

    /**
     * @param void $value
     */
    public function setUsername($value):void
    {
        // TODO: implement here
    }

    /**
     * @return void
     */
    public function getFirstname():void
    {
        // TODO: implement here
        return null;
    }

    /**
     * @param void $value
     */
    public function setFirstname($value):void
    {
        // TODO: implement here
    }

    /**
     * @return void
     */
    public function getLastname():void
    {
        // TODO: implement here
        return null;
    }

    /**
     * @param void $value
     */
    public function setLastname($value):void
    {
        // TODO: implement here
    }

    /**
     * @return void
     */
    public function getEmail():void
    {
        // TODO: implement here
        return null;
    }

    /**
     * @param void $value
     */
    public function setEmail($value):void
    {
        // TODO: implement here
    }

    /**
     * @return void
     */
    public function getPassword():void
    {
        // TODO: implement here
        return null;
    }

    /**
     * @param void $value
     */
    public function setPassword($value):void
    {
        // TODO: implement here
    }

    /**
     * @return void
     */
    public function getActive():void
    {
        // TODO: implement here
        return null;
    }

    /**
     * @param void $value
     */
    public function setActive($value):void
    {
        // TODO: implement here
    }

    /**
     * @return void
     */
    public function getCreatetime():void
    {
        // TODO: implement here
        return null;
    }

    /**
     * @param void $value
     */
    public function setCreatetime($value):void
    {
        // TODO: implement here
    }

    /**
     * @return \UserRoles
     */
    public function getUser_role():\UserRoles
    {
        // TODO: implement here
        return null;
    }

    /**
     * @param \UserRoles $value
     */
    public function setUser_role(\UserRoles $value):void
    {
        // TODO: implement here
    }
}
