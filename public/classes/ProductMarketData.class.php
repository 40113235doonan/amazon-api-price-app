<?php


/**
 *
 */
class ProductMarketData
{
    /**
     *
     */
    public function __construct()
    {
    }

    /**
     * @var void
     */
    public $id;

    /**
     * @var \Product
     */
    public $product;

    /**
     * @var \ProductSource
     */
    public $product_source;

    /**
     * @var void
     */
    public $save_time;

    /**
     * @var void
     */
    public $product_price;

    /**
     *
     */
    public function add():void
    {
        // TODO: implement here
    }

    /**
     *
     */
    public function remove():void
    {
        // TODO: implement here
    }

    /**
     * @return void
     */
    public function getId():void
    {
        // TODO: implement here
        return null;
    }

    /**
     * @param void $value
     */
    public function setId($value):void
    {
        // TODO: implement here
    }

    /**
     * @return \Product
     */
    public function getProduct():\Product
    {
        // TODO: implement here
        return null;
    }

    /**
     * @param \Product $value
     */
    public function setProduct(\Product $value):void
    {
        // TODO: implement here
    }

    /**
     * @return \ProductSource
     */
    public function getProduct_source():\ProductSource
    {
        // TODO: implement here
        return null;
    }

    /**
     * @param \ProductSource $value
     */
    public function setProduct_source(\ProductSource $value):void
    {
        // TODO: implement here
    }

    /**
     * @return void
     */
    public function getSave_time():void
    {
        // TODO: implement here
        return null;
    }

    /**
     * @param void $value
     */
    public function setSave_time($value):void
    {
        // TODO: implement here
    }

    /**
     * @return void
     */
    public function getProduct_price():void
    {
        // TODO: implement here
        return null;
    }

    /**
     * @param void $value
     */
    public function setProduct_price($value):void
    {
        // TODO: implement here
    }
}
