<?php

require __DIR__ . '/../vendor/autoload.php';


use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Pimple\Container;
use Illuminate\Support\Facades\Redirect;
use App\Controllers\ProductController;
use Slim\Views\PhpRenderer;
use App\Controllers\UserController;
use App\Controllers\ProductDataController;
use App\Controllers\ProductSourceController;
use App\Controllers\UserProductController;
use App\Controllers\UserRoleController;
use App\Models\ProductData;
use App\Controllers\AmazonController;

$app = new \Slim\App([
		'settings' => [
				'displayErrorDetails' => true,
				'db' => [
						'driver' => 'mysql',
						'host' => '*****',
						'port' => '3306',
						'database' => 'c1amazonapp',
						'username' => '*******',
						'password' => '******',
						'charset' => 'utf8',
						'collation' => 'utf8_unicode_ci',
						'prefix' => ''
				]
		]
]);

$container = $app->getContainer();


$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);

$capsule->setAsGlobal();
$capsule->bootEloquent();

$container['db'] = function($c) use ($capsule) {

	return $capsule;

};

$container['view'] = function($container) {
	$view = new \Slim\Views\Twig(__DIR__ . '/../resources/views', [
			'cache' => false,
	]);

	$basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
	$view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));

	return $view;
};

$container['renderer'] = new PhpRenderer("./../resources/views");

$app->get('/', function($request, $response, $args) {
	return $this->renderer->render($response, '/layouts/home.php');
});

$app->get('/test/{user_id}', UserController::class . ':getUserSavedProducts');

$container['product'] = function ($c) {
	return new \App\Models\Product;
};

$container['user'] = function ($c) {
	return new \App\Models\User;
};

$container['productdata'] = function ($c) {
	return new \App\Models\ProductData;
};

$container['productsource'] = function ($c) {
	return new \App\Models\ProductSource;
};

$container['userproduct'] = function ($c) {
	return new \App\Models\UserProduct;
};

$container['userrole'] = function ($c) {
	return new \App\Models\UserRole;
};

$container['amazon'] = function ($c) {
	return new \App\Models\Amazon();
};

$app->get('/products', ProductController::class . ':index')->setName('product.list');
$app->get('/products/{asin}', ProductController::class . ':show_single')->setName('product.detail');
$app->delete('/products/{product_id}', ProductController::class . ':delete_single')->setName('product.delete');
$app->post('/products', ProductController::class . ':add_single')->setName('product.add');

$app->get('/error', function($request, $response, $args) { echo "Data exists!"; })->setName('error');


$app->get('/users', UserController::class . ':index')->setName('user.list');
$app->get('/productdata', ProductDataController::class . ':index')->setName('product.data');
$app->post('/productdata/{source}/{product_id}/{price}', ProductDataController::class . ':add')->setName('product.data.add');


$app->get('/productsource', ProductSourceController::class . ':index')->setName('product.source');
$app->get('/userproduct', UserProductController::class . ':index')->setName('user.product');
$app->get('/userrole', UserRoleController::class . ':index')->setName('user.role');

$app->get('/user/products/{username}', UserController::class . ':getUserSavedProducts')->setName('user.products');

$app->get('/productdata/add', function($request, $response) {

	$market = ProductSource::find(1);

	/* $this->product->where('product_id', 20)->marketData()->create([
			'currency' => 'HUF',
	]); */

	return $this->product->where('product_id', '20')->get();

});

$app->get('/amazon/search', AmazonController::class . ':results')->setName('amazon.results');
$app->get('/amazon/search/{keyword}', AmazonController::class . ':search')->setName('amazon.search');






$app->run();
