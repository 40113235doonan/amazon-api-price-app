<?php
require_once 'init.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Price Assistant | Register</title>

    <!-- Bootstrap Core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom CSS -->
    <link href="bootsrap/css/style.css" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <!-- Start PHP Code -->
                    <?php
                        mysql_connect("zedsolutions.com.au:3306", "c1soc09109", "Gr9jSz@9") or die(mysql_error()); // Connect to database server(localhost) with username and password.
                        mysql_select_db("c1amazonapp") or die(mysql_error()); // Select registrations database.

                        if(isset($_POST['username']) && !empty($_POST['username']) AND isset($_POST['firstname']) && !empty($_POST['firstname']) AND isset($_POST['lastname']) && !empty($_POST['lastname']) AND isset($_POST['email']) && !empty($_POST['email']) AND isset($_POST['confirmemail']) && !empty($_POST['confirmemail'])AND isset($_POST['password']) && !empty($_POST['password']) AND isset($_POST['confirmpassword']) && !empty($_POST['confirmpassword'])){
                            // Form Submited
                            $username = mysql_escape_string($_POST['username']); // Turn our post into a local variable
                            $firstname = mysql_escape_string($_POST['firstname']);
                            $lastname = mysql_escape_string($_POST['lastname']);
                            $email = mysql_escape_string($_POST['email']); // Turn our post into a local variable
                            $password = mysql_escape_string($_POST['password']);
		                    $confirmemail = mysql_escape_string($_POST['confirmemail']);
                            $confirmpassword = mysql_escape_string($_POST['confirmpassword']);

		                    $usersearch = mysql_query("SELECT * FROM users WHERE username='".$username."'") or die(mysql_error()); 
                            $usermatch = mysql_num_rows($usersearch);
                            $emailsearch = mysql_query("SELECT * FROM users WHERE email='".$email."'") or die(mysql_error());
                            $emailmatch = mysql_num_rows($emailsearch);
                            
                            if($usermatch > 0) {
                	            $msg = 'That username already exists in our database.';
                            }
                            else if($emailmatch > 0) {
                	            $msg = 'That email address is already registered.';
                            }
		                    else {
                                if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/", $email)){
                                    // Return Error - Invalid Email
                                    $msg = 'The email you have entered is invalid, please try again.';
                                }
		                        else if(strlen($password) < 8) {
			                        $msg = 'Your password must be at least 8 characters.';
		                        }
		                        else if($email != $confirmemail) {
                	                $msg = 'E-mails do not match.';
                                }
                                else if($password != $confirmpassword) {
                	                $msg = 'Passwords do not match.';
                                }
		                        else {
                                    // Return Success - Valid Email
                                    $hash = md5( rand(0,1000) ); // Generate random 32 character hash and assign it to a local variable. Example output: f4552671f8909587cf485ea990207f3b
               
                                    mysql_query("INSERT INTO users (username, firstname, lastname, password, email, hash, user_roles_iduser_roles) VALUES(
                                                '". mysql_escape_string($username) ."',
                                                '". mysql_escape_string($firstname) ."',
                                                '". mysql_escape_string($lastname) ."',
                                                '". mysql_escape_string(md5($password)) ."',
                                                '". mysql_escape_string($email) ."',
                                                '". mysql_escape_string($hash) ."',
			                        			1 ) ") or die(mysql_error());

                                    $to      = $email; // Send email to our user
                                    $subject = 'Signup | Verification'; // Give the email a subject
                                    $message = '

                    Dear '.$firstname.' '.$lastname.',

                    Thanks for signing up!
                    Your account has been created, you can login with the following credentials after you have activated your account by pressing the url below.

                    ------------------------
                    Username: '.$username.'
                    Password: '.$password.'
                    ------------------------

                    Please click this link to activate your account:
                    http://' . $_SERVER['SERVER_NAME'] . '/verify.php?email='.$email.'&hash='.$hash.'

                    Please do not reply to this email
                    
                    '; // Our message above including the link

                                    $headers = 'From:noreply@priceassistant.com' . "\r\n"; // Set from headers
                                    mail($to, $subject, $message, $headers); // Send our email

                                    header("location: registersuccess.php");
                                    die();
                                }
                            }
                        }
                    ?>
                    <!-- End PHP Code -->

                    <div class="panel-heading">
                        <h3 class="panel-title">Registration Form</h3>
                    </div>
                    <div class="panel-body">
                    <!-- Status Message -->
                    <?php
                        if(isset($msg)) {  // Check if $msg is not empty
                            echo '<div class="statusmsg">'.$msg.'</div>';
                        }
                    ?>
                        <form role="form" action="" method="post">
                            <fieldset>
                            <?php
                                if($loggedin == true) {
                                    echo "You are logged in!<br><br>Click <a href='index.php'>here</a> to return to the homepage.<br><br>";
                                }
                                else {
                                    echo '
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Username" name="username" autofocus>
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="First Name" name="firstname">
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Last Name" name="lastname">
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="E-mail" name="email" type="email">
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Confirm E-mail" name="confirmemail" type="email">
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Confirm Password" name="confirmpassword" type="password" value="">
                                        </div>
                                        <!-- Change this to a button or input when using this as a form -->
                                        <input type="submit" class="btn btn-lg btn-success btn-block" value="Register" />';
				                }
			                ?>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>